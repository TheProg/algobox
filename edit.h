#ifndef __EDIT_H__
#define __EDIT_H__
	
	void edit_Algo(AlgoCaract algo);
	void edit_loadBlocks(AlgoCaract algo);
	void edit_addBlock(AlgoCaract *algo, int id, TypeBlock type);
	void edit_addBlock_Var(Block *block, TypeBlock type);
	
#endif