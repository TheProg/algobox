#ifndef __STRUCT_H__
#define __STRUCT_H__
	
	typedef struct
	{
		char name[12];
		int nbrBlocks;
	} AlgoCaract;
	
	typedef enum {VAR_NEW, VAR_READ, VAR_FILL, DISP_VAR, DISP_TXT, COM_IF, COM_FOR, COM_WHILE, COM_END} TypeBlock;
	typedef struct
	{
		TypeBlock type;
		char args[3][20];
	} Block;
	
	typedef enum {BASE, EDIT, VAR, DISP, COM, DEL_L} Menu;
	
	typedef struct
	{
		char name[10];
		char type;
		double value;
	} Variable;
	
#endif