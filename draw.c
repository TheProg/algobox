#include "fxlib.h"
#include "struct.h"
#include "draw.h"
#include "syscalls.h"

#include "MonochromeLib.h"

#include <stdio.h>

void Draw_FKeys(Menu menu)
{
	switch(menu)
	{
		case BASE:
			Draw_FKeySys(0x002A, 0);
			Draw_FKeySys(0x0155, 1);
			Draw_FKeySys(0x0200, 2);
			Draw_FKeySys(0x01FA, 3);
			Draw_FKeySys(0x02AC, 4);
			break;
		case VAR:
			Draw_FKey("NEW", 0, 1);
			Draw_FKey("READ", 1, 1);
			Draw_FKey("FILL", 2, 1);
			break;
		case DISP:
			Draw_FKey("VAR", 0, 1);
			Draw_FKey("TEXT", 1, 1);
			break;
		case COM:
			Draw_FKeySys(0x0204, 0);
			Draw_FKeySys(0x0208, 1);
			Draw_FKeySys(0x020C, 2);
			break;
	}
}

void Draw_FKeySys(int id, int pos)
{
	unsigned char *fKey = NULL;
	GetFKeyIconPointer(id, &fKey);
	DisplayFKeyIcon(pos, fKey);
}

void Draw_FKey(char* title, int pos, int type)
{
	if(type==1)
	{
		ML_rectangle(2+21*pos, 56, 19+21*pos, 63, 1, ML_BLACK, ML_BLACK);
	}
	else
	{
		ML_horizontal_line(56, 2+21*pos, 19+21*pos, ML_BLACK);
		ML_vertical_line(2+21*pos, 56, 63, ML_BLACK);
	}
	
	PrintMini(3+21*pos, 58, title, type+1);
}

void Draw_textCenter(char* string, int y)
{
	int length  = 0;
	int i = 0;
	
	while(string[i] != '\0')
	{
		switch(string[i])
		{
			case 'M': case 'N':
			case 'Q': case 'w':
			case 'W': case 'm':
				length +=6;
				break;
			
			case 'K': case 'n':
			case 'r':
				length  += 5;
				break;
			
			case ':': case '\'':
				length  += 3;
				break;
			
			case 'i':
				length  += 2;
				break;
			
			default:
				length  += 4;
		}
		
		i++;
	}
	
	PrintMini(64 - length /2, y, (unsigned char*)string, 0);
}
