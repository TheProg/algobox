#ifndef __DRAW_H__
#define __DRAW_H__
	
	void Draw_FKeys(Menu menu);
	void Draw_FKeySys(int id, int pos);
	void Draw_FKey(char* title, int pos, int type);
	void Draw_textCenter(char* string, int y);
	
#endif