#include "fxlib.h"
#include "struct.h"
#include "algobox.h"
#include "edit.h"
#include "draw.h"

#include "MonochromeLib.h"
#include "memory.h"

#include <stdlib.h>
#include <stdio.h>


static AlgoCaract *algos = NULL;


int AddIn_main(int isAppli, unsigned short OptionNum)
{
/* DEBUG CHAR SYS
int i = 0;
char buffer[25];

while(1)
{
	i++;
	ML_clear_vram();
	sprintf(buffer, "Char %d: %c", i, i);
	PrintXY(0, 0, buffer, 0);
	ML_display_vram();
	Sleep(1000);
} */
	
	SetQuitHandler(algobox_Quit);
	
	// memory_seterrors(1);
	
	algobox_Start();
	algobox_Menu();
	
	return 1;
}

void algobox_Start()
{
	int i=0;
	
	ML_clear_vram();
	Draw_textCenter("ALGOBOX", 32);
	Draw_textCenter(__DATE__, 59);
	for(; i<40; i++)
	{
		ML_horizontal_line(38, 64+i, 64-i, ML_BLACK);
		ML_display_vram();
		Sleep(25);
	}
	Sleep(500);
}

void algobox_Menu()
{
	unsigned int key;
	char buffer[40];
	const char arrows[2][3] = {{0xE6, 0x92, 0}, {0xE6, 0x93, 0}};
	int nbrAlgos = -1;
	int i, posCursor, posActual;
	unsigned char *fKey = NULL;
	
	nbrAlgos = algobox_LoadCaract();
	posCursor = 0;
	posActual = 0;
	
	while(1)
	{
		ML_clear_vram();
		PrintXY(0, 0, (const unsigned char*)"Liste algos:", 0);
		sprintf(buffer, "%02d/%d", nbrAlgos, 20);
		PrintXY(95, 0, (const unsigned char*)buffer, 0);
		Draw_FKeySys(0x0186, 2); // NEW
		if(nbrAlgos>0)
		{
			Draw_FKeySys(0x0184, 0); // EXEC
			Draw_FKeySys(0x0185, 1); // EDIT
			Draw_FKeySys(0x0038, 3); // DEL
			Draw_FKeySys(0x0104, 4); // DEL-A
		}
		else
		{
			PrintXY(0, 0, (const unsigned char*)"(Pas d'algos)", 0);
		}
		
		for(i=0; i<6 && i<nbrAlgos; i++)
		{
			if(posActual+i == posCursor)
				ML_rectangle(0, 8*(i+1), 128, 8*(i+2)-1, 1, ML_BLACK, ML_BLACK);
			PrintXY(6, 8*(i+1), (unsigned char*)algos[posActual+i].name, (posActual+i == posCursor ? 1 : 0));
			sprintf(buffer, ": %d", algos[posActual+i].nbrBlocks);
			PrintXY(70, 8*(i+1), (unsigned char*)buffer, (posActual+i == posCursor ? 1 : 0));
		}
		if(posActual>0)
			PrintXY(120, 8, arrows[0], (posCursor == posActual ? 1 : 0));
		if(posActual+6<nbrAlgos)
			PrintXY(120, 48, arrows[1], (posCursor == posActual+5 ? 1 : 0));
		
		GetKey(&key);
		
		if(key == KEY_CTRL_F2)
		{
			edit_Algo(algos[posCursor]);
		}
		else if(key == KEY_CTRL_F3)
		{
			nbrAlgos += algobox_AddAlgo();
		}
		else if(key == KEY_CTRL_F4)
		{
			nbrAlgos -= algobox_DeleteAlgo(posCursor);
		}
		else if(key == KEY_CTRL_F5)
		{
			nbrAlgos *= (1-algobox_DeleteAlgos(nbrAlgos));
		}
		else if(key == KEY_CTRL_DOWN && posCursor<nbrAlgos-1)
		{
			posCursor++;
			if(posCursor>posActual+5)
				posActual++;
		}
		else if(key == KEY_CTRL_UP && posCursor>0)
		{
			posCursor--;
			if(posCursor<posActual)
				posActual--;
		}
		
		if(posCursor>nbrAlgos-1)
			posCursor = nbrAlgos-1;
		if(posCursor<0)
			posCursor=0;
	}
}


int algobox_LoadCaract()
{
	char **files;
	char fileName[60];
	int i, j, nbrAlgo, handle;
	
	memory_createdir("\\\\fls0\\ALGOS");
	
	files = memory_alloc(20);
	nbrAlgo = -1;
	nbrAlgo = memory_find("\\\\fls0\\ALGOS\\*.ALG", files, 20);
	
	algos = calloc(nbrAlgo, sizeof(AlgoCaract));
	for(i=0; i<nbrAlgo; i++)
	{
		// Name
		for(j=0; files[i][j] != '.'; j++)
			algos[i].name[j] = files[i][j];
		algos[i].name[j+1] = '\0';
		
		// Size Blocks
		sprintf(fileName, "\\\\fls0\\ALGOS\\%s.ALG", algos[i].name);
		handle = memory_openfile(fileName,_OPENMODE_READ);
		algos[i].nbrBlocks = memory_filesize(handle)/sizeof(Block);
		memory_closefile(handle);
	}
	
	// Free memory
	memory_free(files, 20);
	
	return nbrAlgo;
}

int algobox_AddAlgo()
{
	char name[12] = {0};
	char fileName[40];
	unsigned int key;
	int cursor = 0;
	
	do
	{
		ML_clear_vram();
		PrintXY(0, 0, (const unsigned char*)"Nom algorythme", 0);
		PrintXY(0, 8, (const unsigned char*)"[        ]", 0);
		if(cursor<8)
			PrintXY(6*(cursor+1), 8, (const unsigned char*)"_", 0);
		PrintXY(6, 8, (unsigned char*)name, 0);
		
		GetKey(&key);
		
		if(key >= 'A' && key <= 'Z'  ||  key >= '1' && key <= '9'  ||  key == ' ')
		{
			name[cursor] = key;
			cursor++;
		}
		if(key == KEY_CTRL_DEL  &&  cursor >= 0)
		{
			PrintXY(6, 8, (const unsigned char*)"        ", 0);
			cursor--;
			name[cursor] = 0;
		}
		if(key == KEY_CTRL_EXIT)
			return 0;
		
	}while(key != KEY_CTRL_EXE  ||  cursor == 0);
	
	sprintf(fileName, "\\\\fls0\\ALGOS\\%s.ALG", name);
	if(memory_createfile(fileName, 1)<0)
		return 0;
	
	free(algos);
	algobox_LoadCaract();
	
	return 1;
}

int algobox_DeleteAlgo(int id)
{
	unsigned int key;
	char fileName[60];
	
	PopUpWin(4);
	PrintXY(12, 8, (unsigned char*)"Supprimer", 0);
	PrintXY(46, 16, (unsigned char*)"algorythme?", 0);
	PrintXY(30, 24, (unsigned char*)"Oui:[F1]", 0);
	PrintXY(30, 32, (unsigned char*)"Non:[F6]", 0);
	
	do
	{
		GetKey(&key);
	}while(key!=KEY_CTRL_F1  &&  key!=KEY_CTRL_F6  &&  key!=KEY_CTRL_EXIT);
	
	if(key==KEY_CTRL_F6 || key==KEY_CTRL_EXIT)
		return 0;
	
	sprintf(fileName, "\\\\fls0\\ALGOS\\%s.ALG", algos[id].name);
	memory_deletefile(fileName);
	
	free(algos);
	algobox_LoadCaract();
	
	return 1;
}

int algobox_DeleteAlgos(int nbrAlgos)
{
	unsigned int key;
	char fileName[60];
	int i;
	
	PopUpWin(4);
	PrintXY(12, 8, (unsigned char*)"Supprimer tous", 0);
	PrintXY(16, 16, (unsigned char*)"les algorythmes?", 0);
	PrintXY(30, 24, (unsigned char*)"Oui:[F1]", 0);
	PrintXY(30, 32, (unsigned char*)"Non:[F6]", 0);
	
	do
	{
		GetKey(&key);
	}while(key!=KEY_CTRL_F1  &&  key!=KEY_CTRL_F6  &&  key!=KEY_CTRL_EXIT);
	
	if(key==KEY_CTRL_F6 || key==KEY_CTRL_EXIT)
		return 0;
	
	for(i=0; i<nbrAlgos; i++)
	{
		sprintf(fileName, "\\\\fls0\\ALGOS\\%s.ALG", algos[i].name);
		memory_deletefile(fileName);
	}
	
	free(algos);
	algobox_LoadCaract();
	
	return 1;
}

void algobox_Quit()
{
	free(algos);
}


#pragma section _BR_Size
unsigned long BR_Size;
#pragma section
#pragma section _TOP
int InitializeSystem(int isAppli, unsigned short OptionNum){return INIT_ADDIN_APPLICATION(isAppli, OptionNum);}
#pragma section

