#ifndef __ALGOBOX_H__
#define __ALGOBOX_H__
	
	void algobox_Start();
	void algobox_Menu();
	int algobox_LoadCaract();
	int algobox_AddAlgo();
	int algobox_DeleteAlgo(int id);
	int algobox_DeleteAlgos(int nbrAlgos);
	void algobox_Quit();
	
#endif