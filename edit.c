#include "fxlib.h"
#include "struct.h"
#include "edit.h"

#include "memory.h"
#include "MonochromeLib.h"

#include <stdlib.h>
#include <stdio.h>


static Block *blocks = NULL;
static int nbrVariables = 0;
static Variable *variables = NULL;


void edit_Algo(AlgoCaract algoCaract)
{
	Menu menu = BASE;
	int i;
	int stop = 0;
	unsigned int key;
	
	edit_loadBlocks(algoCaract);
	
	for(i=0; i<algoCaract.nbrBlocks && blocks[i].type == VAR_NEW; i++)
		nbrVariables++;
	
	variables = calloc(nbrVariables, sizeof(Variable));
	
	for(i=0; i<nbrVariables; i++)
	{
		sprintf(variables[i].name, "%s", blocks[i].args[0]);
		variables[i].type = (int)blocks[i].args[1][0];
		variables[i].value = 0.;
	}
	
	do
	{
		ML_clear_vram();
		PrintXY(0, 0, "======        ======", 0);
		PrintXY(36, 0, algoCaract.name, 0);
		Draw_FKeys(menu);
		ML_display_vram();
		
		GetKey(&key);
		
		if(menu == BASE)
		{
			switch(key)
			{
				case KEY_CTRL_F1:
					break;
				case KEY_CTRL_F2:
					menu = VAR;
					break;
				case KEY_CTRL_F3:
					menu = DISP;
					break;
				case KEY_CTRL_F4:
					menu = COM;
					break;
				case KEY_CTRL_F5:
					break;
				case KEY_CTRL_EXIT:
					stop = 1;
			}
		}
		else if(menu == VAR)
		{
			switch(key)
			{
				case KEY_CTRL_F1:
					break;
				case KEY_CTRL_F2:
					break;
				case KEY_CTRL_F3:
					break;
				case KEY_CTRL_EXIT:
					menu = BASE;
			}
		}
		else if(menu == DISP)
		{
			switch(key)
			{
				case KEY_CTRL_F1:
					break;
				case KEY_CTRL_F2:
					break;
				case KEY_CTRL_F3:
					break;
				case KEY_CTRL_EXIT:
					menu = BASE;
			}
		}
		else if(menu == COM)
		{
			switch(key)
			{
				case KEY_CTRL_F1:
					break;
				case KEY_CTRL_F2:
					break;
				case KEY_CTRL_F3:
					break;
				case KEY_CTRL_EXIT:
					menu = BASE;
			}
		}
	}while(!stop);
	
	free(blocks);
	free(variables);
}

void edit_loadBlocks(AlgoCaract algo)
{
	int i, handle;
	char fileName[100];
	
	blocks = calloc(algo.nbrBlocks, sizeof(Block));
	
	if(!algo.nbrBlocks)
		return;
	
	sprintf(fileName, "\\\\fls0\\ALGOS\\%s.ALG", algo.name);
	handle = memory_openfile(fileName, _OPENMODE_READ);
		memory_readfile(handle, blocks, algo.nbrBlocks*sizeof(Block), 0);
	memory_closefile(handle);
}

void edit_addBlock(AlgoCaract *algo, int id, TypeBlock type)
{
	int i, decalage;
	Block *copBlocks = NULL;
	Block newBlock;
	
	switch(type)
	{
		case VAR_NEW:
			id = 0;
		case VAR_READ:
		case VAR_FILL:
			edit_addBlock_Var(&newBlock, type);
			break;
		case DISP_VAR:
		case DISP_TXT:
			//edit_addBlock_Disp(&newBlock);
			break;
		case COM_IF:
		case COM_FOR:
		case COM_WHILE:
			//edit_addBlock_Com(&newBlock);
			break;
	}
	
	copBlocks = calloc(algo->nbrBlocks, sizeof(Block));
	
	for(i=0; i<algo->nbrBlocks; i++)
	{
		copBlocks[i] = blocks[i];
	}
	
	algo->nbrBlocks++;
	
	free(blocks);
	blocks = calloc(algo->nbrBlocks, sizeof(Block));
	
	decalage = 0;
	for(i=0; i<algo->nbrBlocks; i++)
	{
		if(i == id)
		{
			blocks[i+decalage] = newBlock;
			decalage = 1;
		}
		blocks[i+decalage] = copBlocks[i];
	}
	
	free(copBlocks);
}

void edit_addBlock_Var(Block *block, TypeBlock type)
{
	const char header[3][30] = {
		"Nouvelle variable:",
		"Lire variable",
		"Affecter valeur:"
	};
	
	ML_clear_vram();
	PrintXY(0, 0, header[type], 0);
	ML_display_vram();
	Sleep(500);
	
}